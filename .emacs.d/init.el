(org-babel-load-file
 (expand-file-name
  "config.org"
  user-emacs-directory))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("8d3ef5ff6273f2a552152c7febc40eabca26bae05bd12bc85062e2dc224cde9a" "01a5fb324bcb41d69296971171c83830853a38f012ad97e5fb4b0c6d8ad5c29d" "fa1b2c364b1d058d0611caa5f5c9b2e8cdd0eca519ef88af2de2a2728bbf8070" "d824f0976625bb3bb38d3f6dd10b017bdb4612f27102545a188deef0d88b0cd9" "803aaddee599b43da31fb0fd8fae0fa58b4ef7617c673f07201e3463a3099957" "02f57ef0a20b7f61adce51445b68b2a7e832648ce2e7efb19d217b6454c1b644" "944d52450c57b7cbba08f9b3d08095eb7a5541b0ecfb3a0a9ecd4a18f3c28948" default))
 '(package-selected-packages
   '(mood-line mood-one-theme memento-mori doom darktooth-theme dracula-theme simple-httpd exwm-x exwm-modeline exwm xelb python-mode dark-souls writeroom-mode which-key vterm use-package toc-org sudo-edit smex projectile perspective peep-dired org-bullets neotree markdown-mode magit lua-mode ivy-rich ivy-posframe haskell-mode general gcmh evil-tutor evil-collection eshell-syntax-highlighting emojify elfeed-goodies doom-themes doom-modeline dired-open dashboard counsel all-the-icons-dired))
 '(warning-suppress-types '((emacs) (emacs))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
